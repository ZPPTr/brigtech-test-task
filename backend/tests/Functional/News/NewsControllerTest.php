<?php
namespace App\Tests\Functional\News;

use App\Tests\Functional\DbWebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsControllerTest extends DbWebTestCase
{
    const URI = '/api/news';
    const NEWS_CORRECT_DATA = [
        'title' => 'title',
        'content' => 'sh',
    ];

    const NEWS_INCORRECT_DATA = [
        'title' => '',
        'content' => '',
    ];


    public function testIndex()
    {
        $this->client->request('GET', self::URI.'/list');

        $this->assertResponseStatusCodeSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testCreateUnauthorized()
    {
        $this->client->request('POST', self::URI, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode(self::NEWS_CORRECT_DATA));

        $this->assertResponseStatusCodeSame(401, $this->client->getResponse()->getStatusCode());
        $this->assertJson('{"code":401,"message":"JWT Token not found"}', $this->client->getResponse()->getContent());
    }

}
