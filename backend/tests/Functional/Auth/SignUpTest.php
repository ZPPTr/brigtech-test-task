<?php
namespace App\Tests\Functional\Auth;

use App\Tests\Functional\DbWebTestCase;
use http\Client\Response;
use PHPUnit\Util\Json;

class SignUpTest extends DbWebTestCase
{
    private const URI = '/api/sign-up';

    public function testSuccess(): void
    {
        $this->client->request('POST', self::URI, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'email' => 'test-john@app.test',
            'password' => 'password',
        ]));

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());
        self::assertJson('[]', $content);
    }

    public function testNotValid(): void
    {
        $this->client->request('POST', self::URI, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'email' => 'not-email',
            'password' => 'sh',
        ]));

        self::assertEquals(422, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());
        self::assertJson(
            <<<Json
            {
                "errors": [
                    {
                        "property_path":"email",
                        "message":"This value is not a valid email address."
                    },
                    {
                        "property_path":"password",
                        "message":"This value is too short. It should have 3 characters or more."}
                        ]
                    }
Json,
            $content);
    }

    public function testExists(): void
    {
        $this->client->request('POST', self::URI, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'email' => 'email0@email.com',
            'password' => 'password',
        ]));

        self::assertEquals(422, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());
        self::assertJson(
            <<<Json
            {
                "errors": [
                    {
                        "property_path":"email",
                        "message":"User with such email already exists"
                    }
                ]
            }
Json,
            $content);
    }
}
