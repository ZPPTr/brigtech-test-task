<?php
namespace App\Tests\Unit;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AddUserTest extends WebTestCase
{
    public function testTest()
    {
        $this->assertUserCreated();
    }

    private function assertUserCreated()
    {
        self::bootKernel();

        // returns the real and unchanged service container
        $container = self::$kernel->getContainer();

        // gets the special container that allows fetching private services
        $container = self::$container;

        $user = self::$container->get('doctrine')->getRepository(User::class)->findOneByEmail('email0@email.com');
        $this->assertTrue(self::$container->get('security.password_encoder')->isPasswordValid($user, 'password'));
        // ...
    }
}
