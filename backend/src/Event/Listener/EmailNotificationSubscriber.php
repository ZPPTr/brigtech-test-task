<?php
namespace App\Event\Listener;

use App\Event\NewArticleCreated;
use App\Event\NewCommentAdded;
use App\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;
use function foo\func;

class EmailNotificationSubscriber implements EventSubscriberInterface
{
    private \Swift_Mailer $mailer;
    private Environment $twig;

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository, \Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->userRepository = $userRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            NewArticleCreated::class => 'onArticleCreated',
            NewCommentAdded::class => 'onCommentAdded',
        ];
    }

    public function onArticleCreated(NewArticleCreated $event): void
    {
        $commentators = $this->userRepository->fetchCommentatorsEmail();

        $addresses = array_map(function($item) {
            return $item['email'];
        }, $commentators);

        $message = (new \Swift_Message('Task Executor Assignment'))
            ->setTo($addresses)
            ->setBody($this->twig->render('email/new-article-added.html.twig', [
                'articleId' => $event->getArticleId(),
            ]), 'text/html');

        if (!$this->mailer->send($message)) {
            throw new \RuntimeException('Unable to send message.');
        }
    }

    public function onCommentAdded(NewCommentAdded $event): void
    {
        $authorOfArticle = $this->userRepository->findOneBy(['email' => $event->getAuthorIdentifier()]);

        $message = (new \Swift_Message('There was added a new comment to your article'))
            ->setTo([$authorOfArticle->getEmail() => $authorOfArticle->getUsername()])
            ->setBody($this->twig->render('email/new-comment-added.html.twig', [
                'articleId' => $event->getArticleId(),
                'author' => $authorOfArticle,
            ]), 'text/html');

        if (!$this->mailer->send($message)) {
            throw new \RuntimeException('Unable to send message.');
        }
    }
}
