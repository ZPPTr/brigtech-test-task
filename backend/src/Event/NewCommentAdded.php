<?php
namespace App\Event;

class NewCommentAdded
{

    private $authorIdentifier;
    private int $commentId;
    private int $articleId;

    public function __construct($authorIdentifier, int $commentId, int $articleId)
    {
        $this->authorIdentifier = $authorIdentifier;
        $this->commentId = $commentId;
        $this->articleId = $articleId;
    }

    /**
     * @return int
     */
    public function getAuthorIdentifier()
    {
        return $this->authorIdentifier;
    }

    /**
     * @return int
     */
    public function getCommentId(): int
    {
        return $this->commentId;
    }

    /**
     * @return int
     */
    public function getArticleId(): int
    {
        return $this->articleId;
    }
}
