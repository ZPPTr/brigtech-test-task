<?php
namespace App\Event;

class NewArticleCreated
{
    private $authorIdentifier;
    private int $articleId;

    public function __construct($authorIdentifier, int $articleId)
    {
        $this->authorIdentifier = $authorIdentifier;
        $this->articleId = $articleId;
    }

    /**
     * @return int
     */
    public function getAuthorIdentifier()
    {
        return $this->authorIdentifier;
    }

    /**
     * @return int
     */
    public function getArticleId(): int
    {
        return $this->articleId;
    }
}
