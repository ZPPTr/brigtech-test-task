<?php
namespace App\Event;

interface EventDispatcher
{
    public function dispatch(array $events): void;
}
