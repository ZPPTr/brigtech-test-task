<?php
namespace App\Security\Voter;

use App\Entity\Comment;
use App\Entity\News;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class NewsAccess extends Voter
{
    const CREATE = 'create';
    const MANAGE = 'manage';

    private AuthorizationCheckerInterface $security;

    public function __construct(AuthorizationCheckerInterface $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return self::CREATE || ($attribute === self::MANAGE && $subject instanceof News);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
//            var_dump($user->getUsername());
            return false;
        }

        $own = false;


//        print_r($subject);
//        die('afs');
        if ($subject instanceof News) {
            $own = $subject->getAuthor()->getId() === $user->getId();
        }

        switch ($attribute) {
            case self::CREATE:
                return true;
            case self::MANAGE:
                return $own;
        }

        return false;
    }
}
