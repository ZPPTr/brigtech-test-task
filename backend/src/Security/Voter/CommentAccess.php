<?php
namespace App\Security\Voter;

use App\Entity\Comment;
use App\Entity\News;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentAccess extends Voter
{
    const CREATE = 'create';
    const MANAGE = 'manage';

    private AuthorizationCheckerInterface $security;

    public function __construct(AuthorizationCheckerInterface $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return ($attribute === self::CREATE && $subject instanceof News) || ($attribute === self::MANAGE && $subject instanceof Comment);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        $own = false;

        if ($subject instanceof Comment) {
            $own = $subject->getAuthorData()['id'] === $user->getId();
        }

        switch ($attribute) {
            case self::CREATE:
                return true;
            case self::MANAGE:
                return $own;
        }

        return false;
    }
}
