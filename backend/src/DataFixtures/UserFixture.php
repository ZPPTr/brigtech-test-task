<?php
namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixture
 * @package App\DataFixtures
 */
class UserFixture extends Fixture
{
    private UserPasswordEncoderInterface $hasher;

    public function __construct(UserPasswordEncoderInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $em): void
    {
        for ($i = 0; $i <= 10; $i++) {
            $user = $this->createSignUp('email'.$i.'@email.com', 'password');
            $em->persist($user);
        }

        $em->flush();
    }

    private function createSignUp(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email)
            ->setPassword($this->hasher->encodePassword($user, $password))
            ->setRoles([User::ROLE_USER]);

        return $user;
    }
}
