<?php
namespace App\Controller\Api;

use App\Entity\Comment;
use App\Entity\News;
use App\Security\Voter\CommentAccess;
use App\Service\CommentService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment", name="comment")
 */
class CommentController extends AbstractController
{
    private CommentService $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * @Route("/{news_id}", name=".create", methods={"POST"})
     * @Rest\RequestParam(name="content", nullable=false)
     * @ParamConverter("article", options={"id" = "news_id"})
     * @param News $article
     * @param string $content
     * @return View
     */
    public function create(News $article, string $content): View
    {
        $this->denyAccessUnlessGranted(CommentAccess::CREATE, $article);
        $comment = $this->commentService->create($this->getUser(), $article, $content);

        return View::create($comment, Response::HTTP_CREATED);
    }

    /**
     * @Route("/{id}", name=".show", methods={"GET"})
     * @param Comment $comment
     * @return View
     */
    public function show(Comment $comment): View
    {
        return View::create($comment, Response::HTTP_OK);
    }

    /**
     * @Route("/{id}", name=".update", methods={"PATCH"})
     * @Rest\RequestParam(name="content")
     * @param Comment $comment
     * @param string $content
     * @return View
     */
    public function update(Comment $comment, string $content): View
    {
        $this->denyAccessUnlessGranted(CommentAccess::MANAGE, $comment);
        $comment = $this->commentService->update($comment, $content);

        return View::create($comment, Response::HTTP_OK);
    }

    /**
     * @Route("/{id}", name=".delete", methods={"DELETE"})
     * @param Comment $comment
     * @return View
     */
    public function delete(Comment $comment): View
    {
        $this->denyAccessUnlessGranted(CommentAccess::MANAGE, $comment);
        $this->commentService->delete($comment);

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
