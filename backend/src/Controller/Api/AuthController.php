<?php
namespace App\Controller\Api;

use App\DTO\auth\SignUpDto;
use App\Service\UserService;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class AuthController extends AbstractController
{
    private UserService $userService;
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(UserService $userService, Security $security)
    {
        $this->userService = $userService;
        $this->security = $security;
    }

    /**
     * @Route("/sign-up", name="signUp", methods={"POST"})
     * @ParamConverter("dto", converter="fos_rest.request_body")
     * @param SignUpDto $dto
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function index(SignUpDto $dto, ConstraintViolationListInterface $validationErrors): View
    {
        if (count($validationErrors) > 0) {
            return View::create(['errors' => $validationErrors], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $this->userService->signUp($dto);

        return View::create([], Response::HTTP_CREATED);
    }

    /**
     * @Route("/auth/user", name="user", methods={"GET"})
     */
    public function getUser(): View
    {
        return View::create($this->security->getUser(), Response::HTTP_OK);
    }
}
