<?php
namespace App\Controller\Api;

use App\DTO\news\CreateNewsDto;
use App\DTO\news\UpdateNewsDto;
use App\Entity\News;
use App\Security\Voter\NewsAccess;
use App\Service\NewsService;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\View\View;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @Route("/news", name="news")
 */
class NewsController extends AbstractController
{
    private NewsService $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * @Route("/list", name=".list", methods={"GET"})
     * @Annotations\QueryParam(name="perPage", requirements="\d+", nullable=true)
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true)
     * @param int|null $page
     * @param int|null $perPage
     * @return View
     */
    public function all(?int $page = null, ?int $perPage = null): View
    {
        $news = $this->newsService->getNews($page ?? 0, $perPage ?? 5);

        $code = empty($news) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK;

        return View::create($news, $code);
    }

    /**
     * @Route("/{id}", name=".show", methods={"GET"})
     * @param News $article
     * @return View
     */
    public function show(News $article): View
    {
        return View::create($article, Response::HTTP_OK);
    }

    /**
     * @Route("", name=".create", methods={"POST"})
     * @ParamConverter("dto", converter="fos_rest.request_body")
     * @param CreateNewsDto $dto
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function create(CreateNewsDto $dto, ConstraintViolationListInterface $validationErrors): View
    {
        $this->denyAccessUnlessGranted(NewsAccess::CREATE);
        if (count($validationErrors) > 0) {
            return View::create(['errors' => $validationErrors], Response::HTTP_CONFLICT);
        }

        $article = $this->newsService->create($this->getUser(), $dto);
        return View::create($article, Response::HTTP_CREATED);
    }

    /**
     * @Route("/{id}", name=".update", methods={"PATCH"})
     * @ParamConverter("dto", converter="fos_rest.request_body")
     * @param News $article
     * @param UpdateNewsDto $dto
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     */
    public function update(News $article, UpdateNewsDto $dto, ConstraintViolationListInterface $validationErrors): View
    {
        $this->denyAccessUnlessGranted(NewsAccess::MANAGE, $article);
        if (count($validationErrors) > 0) {
            return View::create(['errors' => $validationErrors], Response::HTTP_CONFLICT);
        }

        $article = $this->newsService->update($article, $dto);
        return View::create([$article], Response::HTTP_OK);
    }

    /**
     * @Route("/{id}", name=".delete", methods={"DELETE"})
     * @param News $article
     * @return View
     */
    public function delete(News $article): View
    {
        $this->denyAccessUnlessGranted(NewsAccess::MANAGE, $article);
        $this->newsService->delete($article);

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
