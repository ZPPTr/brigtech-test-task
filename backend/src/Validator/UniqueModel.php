<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueModel extends Constraint
{
    public string $class;
    public array $fields;
    public string $identifier;
    public string $message = 'This value is already used.';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['class', 'fields'];
    }
}
