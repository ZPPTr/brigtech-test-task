<?php

namespace App\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueModelValidator extends ConstraintValidator
{
    /**
     * @var ObjectManager
     */
    private ObjectManager $objectManager;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /** @var UniqueModel $constraint */

        if (!is_array($constraint->fields)) {
            throw new UnexpectedTypeException($constraint->fields, 'array');
        }

        if (0 === count($constraint->fields)) {
            throw new ConstraintDefinitionException('At least one field has to be specified.');
        }

        $repository = $this->objectManager->getRepository($constraint->class);

        if (!$repository) {
            throw new ConstraintDefinitionException(sprintf('Unable to find the repository associated with an entity of class "%s".', $constraint->class));
        }

        $criteria = $this->getCriteria($constraint, $value);

        $result = $repository->findBy($criteria);

        if ($result instanceof \IteratorAggregate) {
            $result = $result->getIterator();
        }

        /* If the result is a MongoCursor, it must be advanced to the first
         * element. Rewinding should have no ill effect if $result is another
         * iterator implementation.
         */
        if ($result instanceof \Iterator) {
            $result->rewind();
        } elseif (is_array($result)) {
            reset($result);
        }

        if (0 === count($result)) {
            return;
        }

        $this->buildViolation($constraint, $result, $criteria);
    }

    /**
     * @param Constraint $constraint
     * @param $value
     * @return array
     */
    private function getCriteria(Constraint $constraint, $value): array
    {
        $class = $this->objectManager->getClassMetadata($constraint->class);
        $criteria = [];

        foreach ($constraint->fields as $fieldName) {
            if (!$class->hasField($fieldName) && !$class->hasAssociation($fieldName)) {
                throw new ConstraintDefinitionException(sprintf('The field "%s" is not mapped by Doctrine, so it cannot be validated for uniqueness.', $fieldName));
            }

            $criteria[$fieldName] = $value->$fieldName;

            if (null !== $criteria[$fieldName] && $class->hasAssociation($fieldName)) {
                /* Ensure the Proxy is initialized before using reflection to
                 * read its identifiers. This is necessary because the wrapped
                 * getter methods in the Proxy are being bypassed.
                 */
                $this->objectManager->initializeObject($criteria[$fieldName]);
            }
        }
        return $criteria;
    }

    /**
     * @param Constraint $constraint
     * @param array $result
     * @param array $criteria
     */
    private function buildViolation(Constraint $constraint, array $result, array $criteria): void
    {
        foreach ($constraint->fields as $modelField => $objectField) {
            $this->context->buildViolation($constraint->message)
                ->setCause($result)
                ->setInvalidValue(implode(', ', $criteria))
                ->atPath($modelField)
                ->addViolation();
        }
    }
}
