<?php
namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, News::class);
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return News[]
     */
    public function getNews(int $page, int $perPage): array
    {
        $db = $this->getEntityManager()->createQueryBuilder();
        $db->select('t')
            ->from($this->getClassName(), 't')
            ->orderBy('t.id', 'DESC')
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $db->getQuery()->getResult();
    }
}
