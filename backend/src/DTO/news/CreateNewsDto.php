<?php
namespace App\DTO\news;

use App\DTO\RequestDtoInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreatePostDto
 * @package App\Dto
 */
class CreateNewsDto implements RequestDtoInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="180")
     */
    private string $title;

    /**
     */
    private string $content;

    public function __construct(Request $request)
    {
        $this->title = ( string ) $request->get('title');
        $this->content = ( string ) $request->get('content');
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function content(): string
    {
        return $this->content;
    }
}
