<?php
namespace App\DTO\auth;

use App\DTO\RequestDtoInterface;
use App\Validator\UniqueModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CredentialsDto
 * @package App\Dto
 * @UniqueModel(class="App\Entity\User", fields={"email": "email"}, message="User with such email already exists")
 */
class SignUpDto implements RequestDtoInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public string $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3")
     */
    private string $password;

    public function __construct(Request $request)
    {
        $this->email = ( string ) $request->get('email');
        $this->password = ( string ) $request->get('password');
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }
}
