<?php
namespace App\Service;

use App\DTO\auth\SignUpDto;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private UserRepository $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository,  UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function signUp(SignUpDto $dto): User
    {
        $user = new User();
        $user->setEmail($dto->email());
        $user->setPassword($this->passwordEncoder->encodePassword($user, $dto->password()));
        $user->setRoles([User::ROLE_USER]);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
