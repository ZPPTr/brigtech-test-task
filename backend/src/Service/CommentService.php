<?php
namespace App\Service;

use App\Entity\Comment;
use App\Entity\News;
use App\Event\NewCommentAdded;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentService
{
    private EntityManagerInterface $em;
    private CommentRepository $commentRepository;
    private EventDispatcherInterface $dispatcher;

    public function __construct(EntityManagerInterface $entityManager, CommentRepository $commentRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $entityManager;
        $this->commentRepository = $commentRepository;
        $this->dispatcher = $eventDispatcher;
    }

    /**
     * @param UserInterface $user
     * @param News $article
     * @param string $commentContent
     * @return Comment
     */
    public function create(UserInterface $user, News $article, string $commentContent): Comment
    {
        $comment = new Comment();
        $comment->setContent($commentContent);
        $comment->setNews($article);
        $comment->setAuthor($user);

        $this->em->persist($comment);
        $this->em->flush();

        $this->dispatcher->dispatch(new NewCommentAdded($user->getUsername(), $comment->getId(), $article->getId()));

        return $comment;
    }

    /**
     * @param Comment $comment
     * @param string $content
     * @return Comment
     */
    public function update(Comment $comment, string $content): Comment
    {
        $comment->setContent($content);
        $this->em->flush();

        return $comment;
    }

    /**
     * @param Comment $comment
     * @return void
     */
    public function delete(Comment $comment): void
    {
        $this->em->remove($comment);
        $this->em->flush();
    }
}
