<?php
namespace App\Service;

use App\DTO\news\CreateNewsDto;
use App\DTO\news\UpdateNewsDto;
use App\Entity\News;
use App\Event\NewArticleCreated;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class NewsService
{
    private EntityManagerInterface $em;
    private NewsRepository $newsRepository;
    private EventDispatcherInterface $dispatcher;

    public function __construct(EntityManagerInterface $entityManager, NewsRepository $newsRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $entityManager;
        $this->newsRepository = $newsRepository;
        $this->dispatcher = $eventDispatcher;
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function getNews(int $page, int $perPage): array
    {
        return $this->newsRepository->getNews($page, $perPage);
    }

    /**
     * @param UserInterface $user
     * @param CreateNewsDto $dto
     * @return News
     */
    public function create(UserInterface $user, CreateNewsDto $dto): News
    {
        $article = new News();
        $article->setTitle($dto->title());
        $article->setContent($dto->content());
        $article->setAuthor($user);

        $this->em->persist($article);
        $this->em->flush();

        $this->dispatcher->dispatch(new NewArticleCreated($user->getUsername(), $article->getId()));

        return $article;
    }

    /**
     * @param News $article
     * @param UpdateNewsDto $dto
     * @return News|null
     */
    public function update(News $article, UpdateNewsDto $dto): News
    {
        $article->setTitle($dto->title() ?? $article->getTitle());
        $article->setContent($dto->content() ?? $article->getContent());
        $this->em->flush();

        return $article;
    }

    /**
     * @param News $article
     */
    public function delete(News $article): void
    {
        $this->em->remove($article);
        $this->em->flush();
    }
}
