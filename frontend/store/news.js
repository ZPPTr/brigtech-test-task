export const state = () => ({
  list: []
})

export const mutations = {
  setList (state, news) {
    state.list = news
  },

  add(state, article) {
    state.list.push(article)
  },

  remove(state, articleId) {
    console.log('mutation articleId', articleId)
    const index = state.list.findIndex(function (item) {
      return item.id === articleId
    })

    state.list.splice(index, 1)
  }
}

export const getters = {
  list: s => s.list
}

export const actions = {
  async fetch({commit}) {
    const news = await this.$axios.get('/api/news/list')
      .then((res) => res.data)

    // const news = [
    //   {
    //     "author_id": 2,
    //     "id": 28,
    //     "title": "Manualy added",
    //     "content": "ASDAsfasdf",
    //     "comments": [],
    //     "created_at": "2020-11-08T19:14:42+00:00",
    //     "updated_at": "2020-11-08T19:14:42+00:00"
    //   },
    //   {
    //     "author_id": 2,
    //     "id": 27,
    //     "title": "asd",
    //     "content": "asdfasdf",
    //     "comments": [],
    //     "created_at": "2020-11-08T18:42:50+00:00",
    //     "updated_at": "2020-11-08T18:42:50+00:00"
    //   },
    //   {
    //     "author_id": 2,
    //     "id": 26,
    //     "title": "FASDFA",
    //     "content": "ASDF",
    //     "comments": [],
    //     "created_at": "2020-11-08T18:38:42+00:00",
    //     "updated_at": "2020-11-08T18:38:42+00:00"
    //   },
    //   {
    //     "author_id": 2,
    //     "id": 25,
    //     "title": "asdf",
    //     "content": "asdfasdf",
    //     "comments": [],
    //     "created_at": "2020-11-08T18:37:47+00:00",
    //     "updated_at": "2020-11-08T18:37:47+00:00"
    //   }
    // ]

    commit('setList', news)
  },

  async create ({commit}, data) {
    const article = await this.$axios.post('/api/news', data)
      .then(res => res.data)
    commit('add', article)
  },

  async remove ({commit}, articleId) {
    console.log('action articleId', articleId)
    await this.$axios.delete('/api/news/' + articleId)
      .catch(error => console.log('Delete article fail', error))

    commit('remove', articleId)
  }
}
