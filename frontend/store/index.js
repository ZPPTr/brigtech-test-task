export const actions = {
  async login({commit}, formData) {
    const response = await this.$auth.loginWith('local', { data: formData })
      .catch(error => console.log('Store login fail', error));
  },
  logout({commit}) {
    commit('clearToken')
  },
}
