up:
	docker-compose up -d nginx
r:
	docker-compose up -d --force-recreate --build nginx
w:
	docker-compose up --force-recreate --build node-start
down:
	docker-compose down
ls:
	docker-compose ps

console:
	docker-compose exec api php bin/console $(c)

eb:
	docker-compose exec backend bash

ef:
	docker-compose exec frontend bash
en:
	docker-compose exec nginx bash


# Frontend
nb:
	cd frontend && npm run build && cd ../
ng:
	cd frontend && npm run generate && cd ../
nbs:
	cd frontend && npm run build && npm run start && cd ../

# Rebuild nuxt in to container
n-build-start:
	docker-compose exec nuxt npm run build npm run start

n-watch:
	docker-compose exec nuxt npm run build npm run dev
