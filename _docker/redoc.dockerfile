########################################################################################################################
# Project documentation
########################################################################################################################
FROM redocly/redoc as redoc

ENV PAGE_TITLE='News Portal API'
ENV SPEC_URL='docs/spec/definitions.yaml'

COPY ./backend/spec /usr/share/nginx/html/spec

# We have to run redoc from subdirectory
RUN ln -s /usr/share/nginx/html/ /usr/share/nginx/html/docs
