FROM node:10-alpine
ENV APP_ROOT /web
ENV NODE_ENV production

WORKDIR ${APP_ROOT}
ADD ./frontend/package.json ${APP_ROOT}
ADD ./frontend/package-lock.json ${APP_ROOT}

RUN chmod 777 * -R

RUN npm ci
RUN npm run build
RUN chmod 777 .nuxt/ -R
#RUN chmod 777 static/ -R

CMD ["npm", "run", "start"]
