########################################################################################################################
# Frontend
########################################################################################################################
FROM node:12.16 as nodejs

ENV APP_ROOT /web

WORKDIR ${APP_ROOT}
ADD ./frontend ${APP_ROOT}

RUN pwd && ls -lah

COPY ./frontend .

RUN npm ci
RUN pwd && ls -lah

# Run production build
#RUN npm run generate
RUN pwd && ls -lah
#RUN chmod .nuxt -r

#CMD ["npm", "run", "start"]


FROM nginx as frontend

#COPY ./backend/spec /usr/share/nginx/html/spec

COPY ./_docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf
#COPY --from=nodejs /web/dist /home/web
