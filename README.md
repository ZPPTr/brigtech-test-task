# BrigTech Test Task
## News manager

### Requirements
- Docker version >= 19.03.13
- Docker-compose version >= 1.21.2

### Setup project
- Enable brain
- Run `docker-compose up -d --force-recreate --build app` or `make recreate`
- It will take a fev minutes
- Check is everything is working. Run `docker-compose ps`
- It must be shown following staff

`api     docker-php-entrypoint php-fpm    Up      9000/tcp                          
 mysql   docker-entrypoint.sh mysqld      Up      0.0.0.0:33061->3306/tcp, 33060/tcp
 nginx   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8088->80/tcp `
 
- Enjoy



### Run tests
- 
 
